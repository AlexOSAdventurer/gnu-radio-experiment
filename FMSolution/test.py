import soundfile as sf
import numpy as np
import functools


"""
        Global variables
samplerate: the wav file's sample rate
spacing: amount of space between each character in the wav file (increased spacing = increased accuracy, decrease in transmitted characters per second)
dataDictMultiplier: the amount of spacing between each of the keys of dataDict. Allows room for error in transmission
"""
samplerate = 32000
spacing = 2
dataDictMultiplier = 50
dataDict = {0:"a", 1:"b", 2:"c", 3:"d", 4:"e", 5:"f", 6:"g", 7:"h", 8:"i", 9:"j", 10:"k", 11:"l", 12:"m", 13:"n", 14:"o", 15:"p", 16:"q", 17:"r", 18:"s", 19:"t", 20:"u", 21:"v", 22:"w", 23:"x", 24:"y", 25:"z", 26:"0", 27:"1", 28:"2", 29:"3", 30:"4", 31:"5", 32:"6", 33:"7", 34:"8", 35:"9", 36:"(", 37:")", 38:",", 39:".", 40:"'", 41:" ", 42:"\n"}

def encodeWav(path, fileName):
    """
    Converts a text file into a wav file
    """
    with open(path, 'r') as f:
        data = []
        for line in f.readlines():
            for char in line:
                data += [list(dataDict.values()).index(char.lower())] * spacing
        f.close()
    sf.write(fileName, np.array(data, dtype='int16'), samplerate, 'PCM_24')



def decodeWav(path):
    """
    decodes a wav file

    """
    with open(path, 'rb') as f:
        data, samplerate = sf.read(f, dtype='int16')
        # data.shape[0]/samplerate
        text = ""
        for item in data:
            text += dataDict[functools.reduce(lambda a,b : a if abs((a * dataDictMultiplier) - (item * dataDictMultiplier)) < abs((b * dataDictMultiplier) - (item * dataDictMultiplier)) else b, dataDict.keys())]
        f.close()
    text = text[::spacing]
    return text


encodeWav('test.txt', 'out.wav')
txt = decodeWav('out.wav')
print(txt)