project2_tx_block.grc is the diagram shown for the FSK solution in the final report. 

bytestobits.c can ... I think the term is "unpack" bits into one bit per byte little endian format, and back again.
Supposed to be used before and after the gnu radio flowchart. 
Arguments are: [option flag] input_file output_file

[option flag] can be either: 

"f" - that means to "unpack" the bits
"b" - that means to reverse the said process. If so, it will also scanf a number, which is supposed to be between 0 and 8. 
For the first byte being read in, it has a counter from 0 to 8 representing the current bit number. The read in number is how much to shift the starting counter from 0. 
This is to compensate for the fact that the first complete block representing a full transmission happens later in the file than the first byte. 
