#include <stdio.h>
#include <stddef.h>

unsigned char byte_conversion[8];

void byte_to_bit_array(unsigned char byte) {
	for (size_t i = 0; i < 8; i++) {
		byte_conversion[i] = (byte & 1);
		byte = byte >> 1;
	}
}

unsigned char bit_array_to_byte() {
	unsigned char res = 0;
	for (size_t i = 0; i < 8; i++) {
		res |= byte_conversion[i];
		res = res << 1;
	}
	return res;
}

void write_bit_array_to_file(FILE* outputfile) {
	for (size_t i = 0; i < 8; i++) {
		fputc((char)(byte_conversion[i]), outputfile);
	}
}

int main(int argc, char** argv) {
	if (argc != 4) {
		printf("Pass in the correct number of arguments.\n");
		return 1;
	}
	FILE* inputfile = fopen(argv[2], "r");
	FILE* outputfile = fopen(argv[3], "w+");
	
	int c;

	if ((argv[1][0] == 'f')) {
		while ((c = fgetc(inputfile)) != EOF) {
			byte_to_bit_array((unsigned char)c);
			write_bit_array_to_file(outputfile);
		}
	}
	else if ((argv[1][0] == 'b')) {
		int counter = 0;
		scanf("%d", &counter);
		while ((c = fgetc(inputfile)) != EOF) {
			byte_conversion[counter] = c;
			counter++;
			if (counter == 8) {
				fputc(bit_array_to_byte(), outputfile);
				counter = 0;
			}
		}
	}
	else {
		printf("Give a proper switch.\n");
		return 1;
	}

	fclose(inputfile);
	fclose(outputfile);

	return 0;
}
